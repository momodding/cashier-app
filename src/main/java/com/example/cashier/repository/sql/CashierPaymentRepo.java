package com.example.cashier.repository.sql;

import com.example.cashier.repository.entity.CashierPayment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Repository
public class CashierPaymentRepo {
    private final EntityManager entityManager;

    public CashierPaymentRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<CashierPayment> findFirstByOrderId(String orderId) {
        String sql = "SELECT * FROM cashier_payment WHERE order_id = :order_id";
        Query query = entityManager.createNativeQuery(sql, CashierPayment.class);
        query.setParameter("order_id", orderId);
        List<CashierPayment> result = (List<CashierPayment>) query.getResultList();

        Optional<CashierPayment> data = Optional.empty();
        for (Iterator iterator = result.iterator(); iterator.hasNext();){
            data = (Optional<CashierPayment>) iterator.next();
        }

        return data;
    }

    public CashierPayment save(CashierPayment cashierPayment) {
        entityManager.persist(cashierPayment);

        return entityManager.merge(cashierPayment);
    }
}
