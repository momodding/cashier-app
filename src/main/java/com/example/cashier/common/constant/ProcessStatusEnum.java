package com.example.cashier.common.constant;

public enum ProcessStatusEnum {
    INIT, PROCESSING, SUCCESS, FAILED, PENDING, DUPLICATE
}
