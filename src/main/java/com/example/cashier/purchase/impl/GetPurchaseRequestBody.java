package com.example.cashier.purchase.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.common.service.Parser;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import org.springframework.stereotype.Service;

@Service
public class GetPurchaseRequestBody implements PurchaseService {

    @Override
    public void execute(PurchaseData purchaseData) {
        PurchaseData data = Parser.parseRequest(purchaseData.getServletRequest(), PurchaseData.class);
        purchaseData.setProductId(data.getProductId());
        purchaseData.setQty(data.getQty());
        purchaseData.setPrice(data.getPrice());
        purchaseData.setTotal(data.getTotal());
        purchaseData.setProcessStatus(ProcessStatusEnum.INIT);
    }
}
