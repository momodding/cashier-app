package com.example.cashier.repository.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "cashier_order")
public class CashierOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "product_id")
    String productId;

    @Column(name = "order_id")
    String orderId;

    @Column(name = "qty")
    String qty;

    @Column(name = "price")
    String price;

    @Column(name = "total")
    String total;

    @Column(name = "purchase_status")
    String purchaseStatus;

    @Column(name = "payment_status")
    String paymentStatus;

    @CreationTimestamp
    @Column(name = "created_at")
    Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    Date updatedAt;
}
