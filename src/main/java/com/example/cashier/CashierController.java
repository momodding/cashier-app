package com.example.cashier;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class CashierController {

    private final ControllerMapper controllerMapper;

    public CashierController(ControllerMapper controllerMapper) {
        this.controllerMapper = controllerMapper;
    }

    @GetMapping("/**")
    public void getHandler(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        controllerMapper.handleRequest(servletRequest, servletResponse);
    }

    @PostMapping("/**")
    public void postHandler(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        controllerMapper.handleRequest(servletRequest, servletResponse);
    }
}
