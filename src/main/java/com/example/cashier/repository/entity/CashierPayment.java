package com.example.cashier.repository.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "cashier_payment")
public class CashierPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "order_id")
    String orderId;

    @Column(name = "payment_type")
    String paymentType;

    @Column(name = "payment_provider")
    String paymentProvider;

    @Column(name = "reference_no")
    String referenceNo;

    @Column(name = "payment_status")
    String paymentStatus;

    @CreationTimestamp
    @Column(name = "created_at")
    Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    Date updatedAt;
}
