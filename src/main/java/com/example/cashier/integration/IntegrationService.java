package com.example.cashier.integration;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "integration")
public interface IntegrationService {
}
