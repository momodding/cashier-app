package com.example.cashier.purchase;

import com.example.cashier.purchase.model.PurchaseData;

public interface PurchaseService {

    public void execute(PurchaseData purchaseData);
}
