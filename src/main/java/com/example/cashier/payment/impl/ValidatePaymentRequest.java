package com.example.cashier.payment.impl;

import com.example.cashier.common.util.AssertUtil;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import org.springframework.stereotype.Service;

@Service
public class ValidatePaymentRequest implements PaymentService {

    @Override
    public void execute(PaymentData paymentData) {
        AssertUtil.notNull(paymentData.getOrderId(), "orderId can't be null");
        AssertUtil.notNull(paymentData.getPaymentType(), "paymentType can't be null");
        AssertUtil.notNull(paymentData.getPaymentProvider(), "paymentProvider can't be null");
    }
}
