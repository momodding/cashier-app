package com.example.cashier.purchase.impl;

import com.example.cashier.purchase.model.PurchaseData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

class GetPurchaseRequestBodyTest {

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnData_whenGivenRequest() throws IOException {
        HttpServletRequest servletRequest = Mockito.mock(HttpServletRequest.class);

//        Map<String, String> requestBody = new HashMap<>();
//        requestBody.put("productId", "1");

        Reader requestBody = new StringReader("{\"productId\": 1}");
        Mockito.when(servletRequest.getReader()).thenReturn(new BufferedReader(requestBody));

        PurchaseData purchaseData = new PurchaseData();
        purchaseData.setServletRequest(servletRequest);

        GetPurchaseRequestBody getPurchaseRequestBody = new GetPurchaseRequestBody();
        getPurchaseRequestBody.execute(purchaseData);
    }
}