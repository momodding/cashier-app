package com.example.cashier.purchase.impl;

import com.example.cashier.common.util.AssertUtil;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import org.springframework.stereotype.Service;

@Service
public class ValidatePurchaseRequest implements PurchaseService {

    @Override
    public void execute(PurchaseData purchaseData) {
        AssertUtil.notNull(purchaseData.getProductId(), "productId can't be null");
        AssertUtil.notNull(purchaseData.getQty(), "qty can't be null");
        AssertUtil.notNull(purchaseData.getPrice(), "price can't be null");
        AssertUtil.notNull(purchaseData.getTotal(), "total can't be null");
    }
}
