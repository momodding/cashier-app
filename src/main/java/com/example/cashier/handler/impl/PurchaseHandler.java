package com.example.cashier.handler.impl;

import com.example.cashier.common.service.Parser;
import com.example.cashier.handler.BusinessHandler;
import com.example.cashier.handler.model.HandlerResponse;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@Service
public class PurchaseHandler implements BusinessHandler {

    private static final String[] CREATE_PURCHASE_PROCESS = new String[] {
        "getPurchaseRequestBody",
        "validatePurchaseRequest",
        "validateStock",
        "generateOrderId",
        "storePurchaseOrder",
        "generatePurchaseResponse",
    };

    private final Map<String, PurchaseService> purchaseServiceMap;

    public PurchaseHandler(Map<String, PurchaseService> purchaseServiceMap) {
        this.purchaseServiceMap = purchaseServiceMap;
    }

    @Override
    public void handle(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        PurchaseData purchaseData = new PurchaseData();
        purchaseData.setServletRequest(servletRequest);

        try {
            Arrays.stream(CREATE_PURCHASE_PROCESS).forEach(process -> {
                purchaseServiceMap.get(process).execute(purchaseData);
            });

            servletResponse.setStatus(HttpStatus.OK.value());
            HandlerResponse response = HandlerResponse.builder()
                    .status(HttpStatus.OK.name())
                    .data(purchaseData.getResponse())
                    .message("Success").build();
            servletResponse.getWriter().write(Parser.parseToString(response));

        } catch (Exception ex) {
            ex.printStackTrace();
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();

            if (ex instanceof IllegalArgumentException) {
                servletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
                HandlerResponse response = HandlerResponse.builder()
                        .status(HttpStatus.BAD_REQUEST.name())
                        .error(stackTraceElement[0].toString())
                        .message(ex.getMessage()).build();
                servletResponse.getWriter().write(Parser.parseToString(response));
            } else {
                servletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                HandlerResponse response = HandlerResponse.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.name())
                        .error(stackTraceElement[0].toString())
                        .message(ex.getMessage()).build();
                servletResponse.getWriter().write(Parser.parseToString(response));
            }
        }


        servletResponse.flushBuffer();
    }
}
