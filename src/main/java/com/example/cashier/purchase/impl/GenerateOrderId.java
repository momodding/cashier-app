package com.example.cashier.purchase.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GenerateOrderId implements PurchaseService {

    @Override
    public void execute(PurchaseData purchaseData) {
        purchaseData.setOrderId(UUID.randomUUID().toString().replace("-", ""));
        purchaseData.setProcessStatus(ProcessStatusEnum.PROCESSING);
    }
}
