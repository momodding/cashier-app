package com.example.cashier.common.util;

public class AssertUtil {

    public static void notNull(Object object, String message) {
        if (null == object) {
            throw new IllegalArgumentException(message);
        }
    }
}
