package com.example.cashier;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "controller")
public class ControllerProps {

    List<ResourceMapping> mapping;

    @Data
    public static class ResourceMapping {
        String url;
        String method;
        String bean;
        String handler;
    }

}
