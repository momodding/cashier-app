package com.example.cashier.payment.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.common.service.Parser;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import org.springframework.stereotype.Service;

@Service
public class GetPaymentRequestBody implements PaymentService {

    @Override
    public void execute(PaymentData paymentData) {
        PaymentData data = Parser.parseRequest(paymentData.getServletRequest(), PaymentData.class);
        paymentData.setOrderId(data.getOrderId());
        paymentData.setPaymentType(data.getPaymentType());
        paymentData.setPaymentProvider(data.getPaymentProvider());
        paymentData.setProcessStatus(ProcessStatusEnum.INIT);
    }
}
