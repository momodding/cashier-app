package com.example.cashier.payment;

import com.example.cashier.payment.model.PaymentData;

public interface PaymentService {
    public void execute(PaymentData paymentData);
}
