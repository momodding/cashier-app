package com.example.cashier.payment.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import com.example.cashier.repository.entity.CashierOrder;
import com.example.cashier.repository.sql.CashierOrderRepo;
import org.springframework.stereotype.Service;

@Service
public class ValidateOrder implements PaymentService {

    private final CashierOrderRepo cashierOrderRepository;

    public ValidateOrder(CashierOrderRepo cashierOrderRepository) {
        this.cashierOrderRepository = cashierOrderRepository;
    }

    @Override
    public void execute(PaymentData paymentData) {
        CashierOrder order = cashierOrderRepository.findFirstByOrderId(paymentData.getOrderId())
            .orElseThrow(() -> {
                paymentData.setProcessStatus(ProcessStatusEnum.FAILED);
                return new IllegalArgumentException("order not found");
            });
        paymentData.setCashierOrder(order);
    }
}
