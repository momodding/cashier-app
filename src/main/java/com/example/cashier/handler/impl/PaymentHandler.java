package com.example.cashier.handler.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.common.service.Parser;
import com.example.cashier.handler.BusinessHandler;
import com.example.cashier.handler.model.HandlerResponse;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@Service
public class PaymentHandler implements BusinessHandler {

    private static final String[] CREATE_PAYMENT_PROCESS = new String[] {
        "getPaymentRequestBody",
        "validatePaymentRequest",
        "validateOrder",
        "validatePayment",
        "createPayment",
        "generatePaymentResponse",
    };

    private static final String[] DUPLICATE_PAYMENT_RESPONSE_PROCESS = new String[] {
        "generatePaymentResponse",
    };

    private final Map<String, PaymentService> paymentServiceMap;

    public PaymentHandler(Map<String, PaymentService> paymentServiceMap) {
        this.paymentServiceMap = paymentServiceMap;
    }

    @Override
    public void handle(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        PaymentData paymentData = new PaymentData();
        paymentData.setServletRequest(servletRequest);
        paymentData.setServletResponse(servletResponse);

        try {
            Arrays.stream(CREATE_PAYMENT_PROCESS).forEach(process -> {
                paymentServiceMap.get(process).execute(paymentData);
            });

            servletResponse.setStatus(HttpStatus.OK.value());
            HandlerResponse response = HandlerResponse.builder()
                    .status(HttpStatus.OK.name())
                    .data(paymentData.getResponse())
                    .message("Success").build();
            servletResponse.getWriter().write(Parser.parseToString(response));

        } catch (Exception ex) {
            ex.printStackTrace();
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();

            if (ex instanceof IllegalArgumentException) {
                handleDuplicatePayment(paymentData, ex);
            } else {
                servletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                HandlerResponse response = HandlerResponse.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.name())
                        .error(stackTraceElement[0].toString())
                        .message(ex.getMessage()).build();
                servletResponse.getWriter().write(Parser.parseToString(response));
            }
        }


        servletResponse.flushBuffer();
    }

    private void handleDuplicatePayment(PaymentData paymentData, Exception ex) throws IOException {
        HttpServletResponse servletResponse = paymentData.getServletResponse();

        if (ProcessStatusEnum.DUPLICATE == paymentData.getProcessStatus()) {
            Arrays.stream(DUPLICATE_PAYMENT_RESPONSE_PROCESS).forEach(process -> {
                paymentServiceMap.get(process).execute(paymentData);
            });
            HandlerResponse response = HandlerResponse.builder()
                    .status(HttpStatus.OK.name())
                    .data(paymentData.getResponse())
                    .message("Success").build();
            servletResponse.getWriter().write(Parser.parseToString(response));
        } else {
            servletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            HandlerResponse response = HandlerResponse.builder()
                    .status(HttpStatus.BAD_REQUEST.name())
                    .error(ex.getMessage())
                    .message(ex.getMessage()).build();
            servletResponse.getWriter().write(Parser.parseToString(response));
        }
    }
}
