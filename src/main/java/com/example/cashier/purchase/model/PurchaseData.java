package com.example.cashier.purchase.model;

import com.example.cashier.common.model.ProcessData;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class PurchaseData extends ProcessData {
    // servlet
    HttpServletRequest servletRequest;

    // data
    @JsonProperty("productId")
    String productId;
    @JsonProperty("qty")
    String qty;
    @JsonProperty("price")
    String price;
    @JsonProperty("orderId")
    String orderId;
    @JsonProperty("total")
    String total;

    // response
    Map<String, Object> response;
}
