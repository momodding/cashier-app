package com.example.cashier.payment.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import com.example.cashier.repository.entity.CashierOrder;
import com.example.cashier.repository.entity.CashierPayment;
import com.example.cashier.repository.sql.CashierOrderRepo;
import com.example.cashier.repository.sql.CashierPaymentRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.concurrent.locks.Lock;

@Service
public class CreatePayment implements PaymentService {

    Logger logger = LoggerFactory.getLogger(LockOrderForPayment.class);

    private static final String ORDER_LOCK_KEY = "order_";

    private final CashierOrderRepo cashierOrderRepository;
    private final CashierPaymentRepo cashierPaymentRepository;
    private final LockRegistry lockRegistry;

    public CreatePayment(CashierOrderRepo cashierOrderRepository, CashierPaymentRepo cashierPaymentRepository, LockRegistry lockRegistry) {
        this.cashierOrderRepository = cashierOrderRepository;
        this.cashierPaymentRepository = cashierPaymentRepository;
        this.lockRegistry = lockRegistry;
    }

    @Override
    @Transactional
    public void execute(PaymentData paymentData) {
        CashierOrder order = paymentData.getCashierOrder();
        Lock lock = lockRegistry.obtain(ORDER_LOCK_KEY + order.getId());
        try {
            if (lock.tryLock()) {
                logger.info("lock successful");
            } else {
                logger.info("lock unsuccessful");
                paymentData.setProcessStatus(ProcessStatusEnum.FAILED);
                throw new IllegalArgumentException("order not found");
            }

            order.setPaymentStatus(ProcessStatusEnum.PROCESSING.name());
            cashierOrderRepository.save(order);

            CashierPayment payment = new CashierPayment();
            payment.setOrderId(paymentData.getOrderId());
            payment.setPaymentType(paymentData.getPaymentType());
            payment.setPaymentProvider(paymentData.getPaymentProvider());
            payment.setPaymentStatus(ProcessStatusEnum.PROCESSING.name());
            cashierPaymentRepository.save(payment);
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        } finally {
            lock.unlock();
        }

        paymentData.setProcessStatus(ProcessStatusEnum.PROCESSING);
    }
}
