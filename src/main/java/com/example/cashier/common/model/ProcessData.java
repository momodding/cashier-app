package com.example.cashier.common.model;

import com.example.cashier.common.constant.ProcessStatusEnum;
import lombok.Data;

@Data
public class ProcessData {
    ProcessStatusEnum processStatus;
}
