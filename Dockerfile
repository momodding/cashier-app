FROM maven:3.6.3-jdk-11-slim AS BUILDER

COPY . .

RUN mvn clean package


FROM eclipse-temurin:11-jdk-alpine
MAINTAINER momodding
#COPY target/cashier-1.0.0.jar cashier-1.0.0.jar
COPY --from=BUILDER  /target/*.jar cashier-svc.jar

ENV DB_HOST=postgres
ENV DB_PORT=5432
ENV DB_NAME=cashier
ENV DB_USERNAME=cashier_app
ENV DB_PASSWORD=root

ENV REDIS_HOST=redis
ENV REDIS_PORT=6379

ENTRYPOINT ["java","-jar","/cashier-svc.jar"]