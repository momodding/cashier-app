package com.example.cashier.payment.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GeneratePaymentResponse implements PaymentService {

    @Override
    public void execute(PaymentData paymentData) {
        Map<String, Object> response = new HashMap<>();
        response.put("orderId", paymentData.getOrderId());
        paymentData.setResponse(response);
        paymentData.setProcessStatus(ProcessStatusEnum.SUCCESS);
    }
}
