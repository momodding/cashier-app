package com.example.cashier;

import com.example.cashier.handler.BusinessHandler;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component
public class ControllerMapper {
    private final ControllerProps controllerProps;
    private final Map<String, BusinessHandler> businessHandlerMap;

    public ControllerMapper(ControllerProps controllerProps, Map<String, BusinessHandler> businessHandlerMap) {
        this.controllerProps = controllerProps;
        this.businessHandlerMap = businessHandlerMap;
    }

    public void handleRequest(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        List<ControllerProps.ResourceMapping> mappings = controllerProps.mapping;

        String pathInfo = servletRequest.getRequestURI();
        String method = servletRequest.getMethod();

        ControllerProps.ResourceMapping resourceMapping = mappings.stream()
                .filter(mapping -> pathInfo.equalsIgnoreCase("/api" + mapping.url) && method.equalsIgnoreCase(mapping.method))
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "entity not found"
                ));

        try {
            businessHandlerMap.get(resourceMapping.bean).handle(servletRequest, servletResponse);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}

