package com.example.cashier.payment.model;

import com.example.cashier.common.model.ProcessData;
import com.example.cashier.repository.entity.CashierOrder;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class PaymentData extends ProcessData {
    // servlet
    HttpServletRequest servletRequest;
    HttpServletResponse servletResponse;

    // data
    @JsonProperty("orderId")
    String orderId;
    @JsonProperty("paymentType")
    String paymentType;
    @JsonProperty("paymentProvider")
    String paymentProvider;
    @JsonProperty("referenceNo")
    String referenceNo;

    // order
    CashierOrder cashierOrder;

    // response
    Map<String, Object> response;

}
