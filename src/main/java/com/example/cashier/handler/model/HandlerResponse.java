package com.example.cashier.handler.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HandlerResponse {
    String status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Object data;
    String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Object error;
}
