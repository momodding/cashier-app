package com.example.cashier.repository.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("cashier_order")
@Getter
@Setter
@ToString
public class OrderCache implements Serializable {
    String id;
    String value;
}
