package com.example.cashier.purchase.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import com.example.cashier.repository.entity.CashierOrder;
import com.example.cashier.repository.sql.CashierOrderRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class StorePurchaseOrder implements PurchaseService {

    private final CashierOrderRepo cashierOrderRepository;

    public StorePurchaseOrder(CashierOrderRepo cashierOrderRepository) {
        this.cashierOrderRepository = cashierOrderRepository;
    }

    @Override
    @Transactional
    public void execute(PurchaseData purchaseData) {
        CashierOrder cashierOrder = new CashierOrder();
        cashierOrder.setOrderId(purchaseData.getOrderId());
        cashierOrder.setProductId(purchaseData.getProductId());
        cashierOrder.setQty(purchaseData.getQty());
        cashierOrder.setPrice(purchaseData.getPrice());
        cashierOrder.setTotal(purchaseData.getTotal());
        cashierOrder.setPurchaseStatus(ProcessStatusEnum.SUCCESS.name());
        cashierOrder.setPaymentStatus(ProcessStatusEnum.INIT.name());
        cashierOrderRepository.save(cashierOrder);
    }
}
