package com.example.cashier.purchase.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.purchase.PurchaseService;
import com.example.cashier.purchase.model.PurchaseData;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GeneratePurchaseResponse implements PurchaseService {

    @Override
    public void execute(PurchaseData purchaseData) {
        Map<String, Object> response = new HashMap<>();
        response.put("orderId", purchaseData.getOrderId());
        purchaseData.setResponse(response);
        purchaseData.setProcessStatus(ProcessStatusEnum.SUCCESS);
    }
}
