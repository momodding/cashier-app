package com.example.cashier.repository.sql;

import com.example.cashier.repository.entity.CashierOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
public class CashierOrderRepo {
    private final EntityManager entityManager;

    public CashierOrderRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<CashierOrder> findFirstByOrderId(String orderId) {
        String sql = "SELECT * FROM cashier_order WHERE order_id = :order_id";
        Query query = entityManager.createNativeQuery(sql, CashierOrder.class);
        query.setParameter("order_id", orderId);
        List<CashierOrder> result = (List<CashierOrder>) query.getResultList();

        Optional<CashierOrder> data = Optional.empty();
        for (CashierOrder cashierOrder : result) {
            data = Optional.of(cashierOrder);
        }

        return data;
    }

    public CashierOrder save(CashierOrder cashierOrder) {
        entityManager.persist(cashierOrder);

        return entityManager.merge(cashierOrder);
    }
}
