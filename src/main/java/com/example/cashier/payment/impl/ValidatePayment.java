package com.example.cashier.payment.impl;

import com.example.cashier.common.constant.ProcessStatusEnum;
import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import com.example.cashier.repository.entity.CashierPayment;
import com.example.cashier.repository.sql.CashierPaymentRepo;
import org.springframework.stereotype.Service;

@Service
public class ValidatePayment implements PaymentService {

    private final CashierPaymentRepo cashierPaymentRepository;

    public ValidatePayment(CashierPaymentRepo cashierPaymentRepository) {
        this.cashierPaymentRepository = cashierPaymentRepository;
    }

    @Override
    public void execute(PaymentData paymentData) {
        CashierPayment payment = cashierPaymentRepository.findFirstByOrderId(paymentData.getOrderId()).orElse(null);

        if (null == payment) {
            return;
        }

        paymentData.setProcessStatus(ProcessStatusEnum.DUPLICATE);
        throw new IllegalArgumentException("payment already created");
    }
}
