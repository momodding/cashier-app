package com.example.cashier.common.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class Parser {

    public static <T> T parseRequest(HttpServletRequest request, Class<T> castToClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String requestBody = IOUtils.toString(request.getReader());
            return objectMapper.readValue(requestBody, castToClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String parseToString(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
