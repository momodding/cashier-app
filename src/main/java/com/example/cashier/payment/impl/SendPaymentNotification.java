package com.example.cashier.payment.impl;

import com.example.cashier.payment.PaymentService;
import com.example.cashier.payment.model.PaymentData;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SendPaymentNotification implements PaymentService {

    public SendPaymentNotification() {}

    @Override
    @Transactional
    public void execute(PaymentData paymentData) {
        System.out.println("send notification to customer");
    }
}
