package com.example.cashier.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface BusinessHandler {
    public void handle(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException;
}
